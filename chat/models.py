from django.db import models
from django.core.validators import RegexValidator

# Create your models here.
class User(models.Model):
    phone_format = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Il formato per il numero di telefono è: '+999999999'. Up to 15 digits allowed.") #formato del numero di telefono
    phone = models.CharField(primary_key=True, validators=[phone_format], max_length=17) #telefono dell'utente. Sarà la chiave primaria
    password = models.CharField(max_length=36)  # password dell'utente
    name = models.CharField(max_length=30)
    surname = models.CharField(max_length=30)
    def __str__(self):
        return self.phone #la stringa restituita dalla classe
    class Meta:
        verbose_name_plural = "Users" #indica a django quale è il nome plurale della classe (utile nella sezione admin)


class Rubrica(models.Model):
    phone_format = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Il formato per il numero di telefono è: '+999999999'. Up to 15 digits allowed.")  # formato del numero di telefono
    phone_pr = models.CharField(validators=[phone_format], max_length=17)  # telefono
    contatto = models.CharField(validators=[phone_format], max_length=17)  # telefono
    def __str__(self):
        return self.phone_pr #la stringa restituita dalla classe
    class Meta:
        unique_together = (("phone_pr", "contatto"),)
        verbose_name_plural = "Rubriche" #indica a django quale è il nome plurale della classe (utile nella sezione admin)


class ChatRoom(models.Model):
    name = models.CharField(max_length=30)
    create_at = models.DateField() #data di creazione della chat
    phone_format = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Il formato per il numero di telefono è: '+999999999'. Up to 15 digits allowed.")  # formato del numero di telefono
    creator = models.CharField(validators=[phone_format], max_length=17)  # telefono
    def __str__(self):
        return self.name #la stringa restituita dalla classe
    class Meta:
        verbose_name_plural = "Chat rooms" #indica a django quale è il nome plurale della classe (utile nella sezione admin)


class Partecipante(models.Model):
    phone_format = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Il formato per il numero di telefono è: '+999999999'. Up to 15 digits allowed.")  # formato del numero di telefono
    phone = models.CharField(validators=[phone_format], max_length=17)  # telefono
    chat_room_id = models.IntegerField()
    def __str__(self):
        return self.phone #la stringa restituita dalla classe
    class Meta:
        verbose_name_plural = "Partecipanti" #indica a django quale è il nome plurale della classe (utile nella sezione admin)


class Message(models.Model):
    message = models.CharField(max_length=150)
    chat_room_id = models.IntegerField()
    phone_format = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Il formato per il numero di telefono è: '+999999999'. Up to 15 digits allowed.")  # formato del numero di telefono
    scritto_da = models.CharField(validators=[phone_format], max_length=17)  # telefono
    def __str__(self):
        return u'%s' % (self.chat_room_id) #la stringa restituita dalla classe
    class Meta:
        verbose_name_plural = "Messages" #indica a django quale è il nome plurale della classe (utile nella sezione admin)

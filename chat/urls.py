from django.urls import path
#from django.conf.urls import include, url

from . import views, forms

urlpatterns = [
    path('home/', views.home, name='home'),
    path('signin/', views.signin, name='signin'),
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
    path('rubrica/', views.rubrica, name='rubrica'),
    path('del_contact/<int:contact>', views.del_contact, name='del_contact'),
    path('nuova_chat/', views.nuova_chat, name='nuova_chat'),
    path('crea/<int:partecipante>', views.crea, name='crea'),
    path('add_partecipante/<int:chat_room_id>/<int:partecipante>', views.add_partecipante, name='add_partecipante'),
    path('nuovo_gruppo/', views.nuovo_gruppo, name='nuovo_gruppo'),
    path('del_chat/<int:chat_room_id>', views.del_chat, name='del_chat'),
    path('chat/<int:chat_room_id>/', views.chat, name='chat'),
]
from django.test import TestCase

from .models import * #importo il models
from .forms import * #importo il form

# Create your tests here.
class TestSigninUser(TestCase):
    @classmethod
    def setUp(self):
        User.objects.create(phone="3409080777", password="000000",
                            name="Gianni", surname="Sensational")

    def test_phone_label(self):
        utente = User.objects.get(phone="3409080777")
        self.assertEquals(utente.phone, "3409080777")

    def test_phone_not_valid(self):
        data = {'phone': "cane", 'password': "000000",
                'name': "Marion", 'surname': "Tassozero"} #telefono con formato sbagliato
        form = RegisterForm(data=data) #test del form di registrazione
        self.assertFalse(form.is_valid())

    def test_phone_empty(self):
        data = {'phone': "", 'password': "000000",
                'name': "Marion", 'surname': "Tassozero"} #campo telefono vuoto
        form = RegisterForm(data=data) #test del form di registrazione
        self.assertFalse(form.is_valid())

class TestLoginUser(TestCase):
    @classmethod
    def setUp(self):
        User.objects.create(phone="3409080777", password="000000",
                            name="Gianni", surname="Sensational")

    def test_phone_label(self):
        utente = User.objects.get(phone="3409080777")
        self.assertEquals(utente.phone, "3409080777")

    def test_phone_not_valid(self):
        data = {'phone': "cane", 'password': "000000"} #telefono con formato sbagliato
        form = LoginForm(data=data) #test del form di login
        self.assertFalse(form.is_valid())

    def test_user_psw_errati(self):
        utente = User.objects.get(phone="3409080777")
        self.assertFalse(int(utente.password), "123456")
from django import forms

from django.core.validators import RegexValidator
from .models import User, Message, Rubrica

class RegisterForm(forms.Form):
    phone_format = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Il formato per il numero di telefono è: '+999999999'. Up to 15 digits allowed.")  # formato del numero di telefono
    phone = forms.CharField(max_length=17, validators=[phone_format], widget=forms.TextInput(attrs={'placeholder': 'Phone'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Password'}), max_length=20)
    name = forms.CharField(label='', widget=forms.TextInput(attrs={'placeholder': 'Nome'}))
    surname = forms.CharField(label='', widget=forms.TextInput(attrs={'placeholder': 'Cognome'}))
    class Meta:
        model = User

class LoginForm(forms.Form):
    phone_format = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Il formato per il numero di telefono è: '+999999999'. Up to 15 digits allowed.")  # formato del numero di telefono
    phone = forms.CharField(max_length=17, validators=[phone_format], widget=forms.TextInput(attrs={'placeholder': 'Phone'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Password'}), max_length=20)
    class Meta:
        model = User
        fields = ('phone', 'password',)

class NewContattoForm(forms.Form):
    new_contatto = forms.CharField(label='', widget=forms.TextInput(attrs={'placeholder': 'Numero contatto'}))
    class Meta:
        model = Rubrica

class MessageForm(forms.Form):
    message = forms.CharField(label='', widget=forms.TextInput(attrs={'placeholder': 'Messaggio'}))
    class Meta:
        model = Message

class GroupForm(forms.Form):
    group_name = forms.CharField(label='', widget=forms.TextInput(attrs={'placeholder': 'Nome gruppo'}))
    user = forms.BooleanField(required= False)


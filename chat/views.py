from django.http import HttpResponse
from django.shortcuts import render, redirect

from django.http import HttpResponseRedirect
from django.urls import reverse

from datetime import datetime
from django.utils import formats

from .models import * #importo il models
from .forms import * #importo il forms

# Create your views here.
def home(request): #home page
    if 'user_id' in request.session:  # controllo se l'utente esiste nella sessione
        user_logged = request.session['user_id']  # acquisisco l'utente loggato
        partecip = Partecipante.objects.filter(phone=user_logged).values('chat_room_id') #ottengo l'id delle chat in cui partecipa l'utente loggato
        chats = ChatRoom.objects.filter(id__in=partecip) #ottengo le chat in cui l'utente loggato vi partecipa
        return render(request, 'home.html', {'user_logged': user_logged, 'chats': chats})  # reinirizzo alla home
    return render(request, 'home.html', {})  # reinirizzo alla home

def signin(request): #registrazione
    if request.method == "POST":
        register_form = RegisterForm(request.POST)
        if register_form.is_valid():
                register_user = User(phone=request.POST['phone'], password=request.POST['password'], name=request.POST['name'], surname=request.POST['surname'])
                register_user.save()
                request.session['user_id'] = register_user.phone  # salvo l'utente nella sessione
                return HttpResponseRedirect(reverse('home'))  # reinirizzo alla home
        else:
            return render(request, 'signin.html', {'register_form': register_form, 'errore': "i dati inseriti non sono corretti"})
    else:
        register_form = RegisterForm()  # svuoto il form
    return render(request, 'signin.html', {'register_form': register_form})  # reinirizzo alla home

def login(request): #effettua il login
    if request.method == "POST":
        login_form = LoginForm(request.POST)
        if login_form.is_valid():
            try:
                temp_phone = User.objects.get(phone=request.POST['phone']) #verifico il num di tel
                if temp_phone.password == request.POST['password']: #verifico la password
                    request.session['user_id'] = temp_phone.phone #salvo l'utente nella sessione
                    return HttpResponseRedirect(reverse('home')) #reinirizzo alla home
            except Exception as e:
                print (e)
                return render(request, 'login.html', {'login_form': login_form, 'errore': e})  # reindirizzo al login
        else:
            return render(request, 'login.html', {'login_form': login_form, 'errore':  "i dati inseriti non sono corretti"})  # reindirizzo al login
    else:
        login_form = LoginForm() #svuoto il form
    return render(request, 'login.html', {'login_form': login_form}) #reindirizzo al login

def logout(request): #effettua il logout
    try:
        del request.session['user_id'] #elimino la sessione
    except KeyError:
        pass
    return HttpResponseRedirect(reverse('home')) # reinirizzo alla home

def rubrica(request): #visualizza la rubrica e aggiunge un nuovo contatto
    user_logged = request.session['user_id']
    if user_logged:
        contacts=Rubrica.objects.filter(phone_pr=user_logged).values() #ottengo la lista di tutti i contatti dell'utente loggato
        if request.method == "POST":
            new_contatto_form = NewContattoForm(request.POST)
            if new_contatto_form.is_valid():
                try:
                    User.objects.get(phone=new_contatto_form.cleaned_data['new_contatto'])  # verifico se il num di tel esiste in User
                    new_contatto=Rubrica(phone_pr=user_logged, contatto=new_contatto_form.cleaned_data['new_contatto'])
                    new_contatto.save() # salvo il numero in Rubrica
                    new_contatto_form = NewContattoForm()  # svuoto il form
                    return render(request, 'rubrica.html', {'user_logged': user_logged, 'contacts': contacts, 'new_contatto_form': new_contatto_form})  # reinirizzo alla rubrica
                except Exception as e:
                    new_contatto_form = NewContattoForm() #svuoto il form
                    return render(request, 'rubrica.html', {'user_logged': user_logged, 'contacts': contacts, 'new_contatto_form': new_contatto_form, 'errore': e})  # reinirizzo alla rubrica
        else:
            new_contatto_form = NewContattoForm() #svuoto il form
        return render(request, 'rubrica.html', {'user_logged': user_logged, 'contacts': contacts, 'new_contatto_form': new_contatto_form}) # reinirizzo alla rubrica
    return HttpResponseRedirect(reverse('home')) # reinirizzo alla home

def del_contact(request, contact): #elimina un contatto dalla rubrica
    user_logged = request.session['user_id']
    if user_logged:
        Rubrica.objects.filter(contatto=contact, phone_pr=user_logged).delete() #elimina il contatto selezionato dell'utente loggato
        return HttpResponseRedirect(reverse('rubrica')) #reindirizzo a rubrica
    return HttpResponseRedirect(reverse('home')) # reinirizzo alla home

def nuova_chat(request): #crea una nuova chat
    user_logged = request.session['user_id']
    if user_logged:
        contacts = Rubrica.objects.filter(phone_pr=user_logged).values() # ottengo la lista di tutti i contatti dell'utente loggato
        return render(request, 'nuova_chat.html',{'user_logged': user_logged, 'contacts': contacts}) # reinirizzo alla pagina di creazione della chat
    return HttpResponseRedirect(reverse('home')) # reinirizzo alla home

def crea(request, partecipante): #funzione di appogio per la creazione di una chat
    user_logged = request.session['user_id']
    formatted_datetime = formats.date_format(datetime.now(), 'Y-m-d')  # ottengo la data odierna e la formato
    try:
        new_chat_room = ChatRoom(name='Nome Nuova Chat', create_at=formatted_datetime, creator=user_logged)  # inserisco i dati per la nuova chat
        new_chat_room.save()  # salvo la chat ne db
        obj = ChatRoom.objects.only('id').order_by('-id')[0] #ottengo il record della chat appena creata (ovvero l'ultimo record inserito nel db)
        new_partecipante_creator = Partecipante(phone=user_logged, chat_room_id=obj.id) #inserisco i dati del creatore della chat
        new_partecipante_creator.save() #salvo il creatore della chat nel db
        new_partecipante = Partecipante(phone=partecipante, chat_room_id=obj.id)  # inserisco i dati del partecipante della chat
        new_partecipante.save() #salvo il partecipante della chat nel db
        return HttpResponseRedirect(reverse('chat', kwargs={'chat_room_id': obj.id}))  # reinirizzo alla chat appena creata
    except Exception as e:
        print (e)
        return HttpResponseRedirect(reverse('nuova_chat'))  # reinirizzo alla creazione della chat

def add_partecipante(request, chat_room_id, partecipante):
    new_partecipante=Partecipante(phone=partecipante, chat_room_id=chat_room_id)
    new_partecipante.save()
    return HttpResponseRedirect(reverse('chat', kwargs={'chat_room_id': chat_room_id}))  # reinirizzo alla chat appena creata

def nuovo_gruppo(request): #crea un nuovo gruppo
    user_logged = request.session['user_id']
    formatted_datetime = formats.date_format(datetime.now(), 'Y-m-d')  # ottengo la data odierna e la formato
    if user_logged:
        contacts = Rubrica.objects.filter(phone_pr=user_logged).values()  # ottengo la lista di tutti i contatti dell'utente loggato
        if request.method == "POST":
            check_form = GroupForm(request.POST)
            if check_form.is_valid():
                new_chat_room = ChatRoom(name=request.POST['group_name'], create_at=formatted_datetime, creator=user_logged)  # inserisco i dati per la nuova chat
                new_chat_room.save()  # salvo la chat ne db
                obj = ChatRoom.objects.only('id').order_by('-id')[0]  # ottengo il record della chat appena creata (ovvero l'ultimo record inserito nel db)
                new_partecipante_creator = Partecipante(phone=user_logged, chat_room_id=obj.id)  # inserisco i dati del creatore della chat
                new_partecipante_creator.save()  # salvo il creatore della chat nel db
                for x in range(0,3): #scorro tutte le checkbox
                    checkbox_phone = request.POST.get(str(x)) #ottengo il valore di ogni singola checkbox
                    if checkbox_phone: #se la checkbox è stata selezionata conterrà il numero di telefono
                        new_partecipante = Partecipante(phone=checkbox_phone, chat_room_id=obj.id) #ad ogni ciclo carico il contatto selezionato in Partecipanti
                        new_partecipante.save()  # salvo il partecipante della chat nel db
                return HttpResponseRedirect(reverse('chat', kwargs={'chat_room_id': obj.id}))  # reinirizzo alla chat appena creata

        else:
            check_form = GroupForm()  # svuoto il form
        return render(request, 'nuovo_gruppo.html',{'user_logged': user_logged, 'contacts': contacts, 'check_form': check_form})  # reinirizzo alla rubrica
    return HttpResponseRedirect(reverse('home'))  # reinirizzo alla home

def del_chat(request, chat_room_id): #elimina una chat dalla chat room
    user_logged = request.session['user_id']
    if user_logged:
        ChatRoom.objects.filter(id=chat_room_id).delete() #elimina la chat selezionata
        Partecipante.objects.filter(chat_room_id=chat_room_id).delete() #elimino i partecipanti legati alla chat
    return HttpResponseRedirect(reverse('home')) # reinirizzo alla home

def chat(request, chat_room_id): #visualizza la chat e invia i messaggi
    user_logged = request.session['user_id']
    if user_logged:
        chat_name = ChatRoom.objects.get(pk=chat_room_id) #ottengo il nome della chat
        partecipanti = Partecipante.objects.filter(chat_room_id=chat_room_id)
        contacts = Rubrica.objects.filter(phone_pr=user_logged).values()  # ottengo la lista di tutti i contatti dell'utente loggato
        messages = Message.objects.filter(chat_room_id=chat_room_id) #ottengo i messaggi di questa chat
        if request.method == "POST":
            message_form = MessageForm(request.POST)
            if message_form.is_valid():
                message = Message(message=request.POST['message'], chat_room_id=chat_room_id, scritto_da=user_logged) #inserisco il messaggio associato alla chat room e a chi l'ha scritto
                message.save()#salva il messaggio
                return HttpResponseRedirect(reverse('chat', kwargs={'chat_room_id': chat_room_id})) #reindirizzo alla chat
        else:
            message_form = MessageForm()  # svuoto il form
        return render(request, 'chat.html', {'user_logged': user_logged, 'chat_name': chat_name, 'messages': messages, 'message_form': message_form, 'contacts': contacts, 'chat_room_id': chat_room_id, 'partecipanti': partecipanti})  # reinirizzo alla chat
    else:
        return HttpResponseRedirect(reverse('home'))  # reinirizzo alla home
    return render(request, 'home.html', {})  # reinirizzo alla home

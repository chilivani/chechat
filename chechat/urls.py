from django.contrib import admin
from django.urls import path, include

from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('hello/', views.hello),
    path('admin/doc/', include('django.contrib.admindocs.urls')),
    path('', include('chat.urls')),
]
